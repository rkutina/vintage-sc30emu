#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <curses.h>     //replace with pdcurses on Windows

#include "periint.h"	//peripheral interface


                                            // @@@@@@@@@@@@                            @@@@@ 
       // @@@@                            @@@@@@@@@@@@@@@@@@@@@@                     @@@@@@@@
      // @@@@@@@@                      @@@@@@                @@@@@@                @@@@````@@
      // @@```@@@@@@@@@@@           @@@@@                        @@@@            @@@@```@@@@@
     // @@``@@```@@@@@@@@@@@@@    @@@@ @@@                   @@@@  @@@@@       @@@@```@@``@`@@
     // @@`@```@```@@`````@@@@@@@@@@  @@`@@                @@@@@@     @@@@    @@@````@````@`@@
     // @@`@`````@```@````````@@@@    @@``@@@           @@@@@``@@       @@@ @@@`````@`````@`@@
      // @`@``````@@```````````@@@@@  @@```@@@       @@@@@@```@@         @@@@@`````@``````@`@@
      // @@@````````@`````````@@``@@@@@@`````@@     @@@@``````@@      @@@@@@@`````@```@@@`@`@@
      // @@@`````````@```````@@`````@@@@``````@@  @@@@```````@@     @@@@@```@@````@`@@``@`@`@@
      // @@```````````@``````@@```````@@@``````@@@@@````````@@  @@@@@@``````@@```@``````@`@`@@
      // @@`@`@@@@@````@````@@``````````@``````@@@@````````@@@@@@@@@`````````@@`@``````@``@`@@
      // @@`@`@`````````@@`@@````````````@``````@`````````@@@@@@``@@@@````````@@`````@````@`@@
      // @@`@``@``````````@@@`````````````@`````@``````````````````````@@@@@@@@@``````@```@`@@
      // @@`@```@`````````@@```````````````````````````````````````````````````@@``````@``@@@
       // @@``````@``````@@`@````````````````````````````````````````````````@@`@@````````@@@
       // @@`@```@``````@@```@``````````    SPACECAT#30 EMULATOR    ```````@@@```@@`@@``@``@@
       // @@`@`````````@@``````@```````` -------------------------- ````@@@`@`````@@@``@`@`@@
       // @@````@```@`@@````````@```````   A silly emulator of a    ```@@@@@`````@`@@````@@@
        // @@`@`@``@@@@``````````@@@```` silly 32-bit architecture. ``````@@`````@``@@@```@@
        // @@`@`@@@`@@``@``````@@@``````                            `````````@````@`@@@@@`@@
         // @@````@@@```@```@@`````````` Implemented by             ``````````@````@@`@@@@@
         // @@`@`@@@`@``@``@````````````    Richard Spacecat KUTINA ```````````@```````@@@@
         // @@`@@@@```@@``@`````````````                            ````````````@``````@`@@
          // @@@@`````````@@@````````````````````````````````````````````````````@````@`@@@
          // @@@````````````@```````````                            ``````````````@@``@`@@@@
          // @@@`@`````````@```````````` This piece of code should  ````````````````@@`@@ @@
          // @@@``@```````@````````````` be distributed under the   `````````````````@`@@  @@
          // @ @@````````@`````````````` zlib license found below.  ````````````@````@@@   @@
         // @@  @@`@````@```````````````                            `````````````@...@`@@    @
         // @@  @@@`@```@`@@``````````````````````````````````````````@@@````````@.@`@@     @@
         // @    @@``@````@`````````````````````````````````````````@  @@@@``````@@`@@@     @@
        // @@     @@``@``@`````````````````@@  @@@`````````````````@   @ @@@`````@@`@@       @
        // @@      @@``@`````````````````@@   @ @@@```````````````@    @ @@@`````@`@@        @@
        // @@       @@``@````````````````@    @ @@@```````````````@    @@@@ @````@@@         @@
        // @         @@`````````````````@     @@@@ @``````````````@    @@@@ @````@@@         @@
        // @          @@```````````````@      @@@@ @``````````````     @@@@ @````@@          @@
       // @@          @@```````````````@      @@@@  @`````````````@     @@@  @````@@          @
       // @@          @@```````````````       @@@@  @`````````````@     @@   @````@@          @
       // @@          @@```````````````@      @@@@  @`````````````@         @`````@@          @
       // @@          @@```````````````@       @@    @`````````````@       @``````@@          @@
       // @@          @@````````````````@           @@``````````````@@@@@@@````````@@         @@
       // @@          @@`````````````````@         @@@````````````````@@@``````````@@         @@
       // @@          @@```````````````````@@@@@@@@`````````````````````@@``````````@@        @@
       // @@         @@``````````````````````````````````````````````````@@`````````@@        @@
       // @@         @@```````````````````````````````````````````````````@@`````````@@       @@
       // @@         @@````````````````````````````````````````````````````@@````````@@       @
       // @@        @@``````````````````````````````````````````````````````@@````````@@      @
       // @@        @@`````` Spacecat emuSC30 20150402 ``````@@,@@@@@@@,@```@@```````@@      @
        // @       @@@`````` ~~~~~~~~~~~~~~~~~~~~~~~~~ ``````@,,,,,,,,,,,@````@````````@@    @@
        // @       @@``````` spacecatindustries.org    ```````@@@@,,,,,,@@````@````````@@    @@
        // @@      @@``````` kutinar@gmail.com         ````````@++,,,,,,@`````@`````@@`@@    @@
        // @@     @@````````                           ``````````@@,,,,,@@`````@``````@@@@    @
        // @@    @@@`````````````````````````````````````````````@@@,@@```````@```````@@     @
         // @    @@`````````````````````````````````````````````````@````````@@```````@@    @@
         // @@   @@``````````````````````````````````````````````````````````@@```````@@    @@
         // @@  @@```@```````````````````````@```````````````````````````````@@```````@@    @
          // @@ @@``@@``````````````````````@@@@`````````````````````````````@`````````@@  @@
          // @@ @@@@@@`````````````````````@@@@@@```````````````````````````@@`````````@@  @@
          // @@ @@@@@``````````````````````@@@@@@@``````````````````@``````@@``````````@@ @@
           // @@   @@``````````````````````@@@@@@@@@```````````````@@@```@@ @```````@``@@ @@
           // @@   @@``````````````````````@@@@@@@@@@@@``````````@@@@@@@@@  @```````@@@@ @@
            // @@  @@``````````````````````@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@```````@@@@ @@
            // @@  @@```````````````````````@@@@@@@@@@@@@@@@  @@@@@@@@@@@@ @@```````@@  @@
             // @@ @@```@```````````````````@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@@````````@@  @@
              // @@@@```@```````````````````@@@RAWRAWRAWRAWR@ @@@@@@@@@@@@@@````@``@@  @@
              // @@@@``@@````````````````````@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@````@``@@ @@
               // @@@@`@@````````````````````@@@@@@@@@@@@@@@@@,,@@,,@@@@@@@@````@``@@ @@
                // @@@@@@@```@````````````````@@@@@@@@@@@@@,,,,,,,,,@,,@@ @@````@@@@ @@
                 // @@@ @@```@`````````````````@@@@@@@@@@,,,,,,,,@,,,,,,@ @````@@@@ @@
                 // @@@ @@``@@``````````````````@@@@@@@@,,,,,,,,@ @,,,,,@@`````@@  @@
                  // @@ @@``@@```````````````````@@@@@,,,,,,,,,,@ @,,,,@@`````@@  @@
                   // @@@@@`@@@````````````````````@@@,,,,,,,,,,@ @,@@@````@@`@@ @@@
                    // @@@@@@@@``@`````````````````````@@@@@@@@@@@@@``````@@@@@ @@@
                     // @@@@  @@`@@`````````````````````````````````````@@@@@@ @@
                      // @@@  @@@@`@`````````````````````````````````@@@``@@  @@
                        // @@ @@`@```@``````````@@@@```````@@@@@@@@@```````@@@@
                         // @@@@```````````````````````````````````````````@@@@
                         // @@@@@```````````````````````````````````````@@@@@@@
                         // @@'''@@@@@@```````````````````````````@@@@@@''''''@@
                         // @@@,@@@''@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'''''@@@,@@@
                        // @@'@@,,,@@@@@@''''''''''@@@''''''''''''@@@@@@,,,,@@@@
                        // @@'''@@@@,,,,,,,@@@@@@@@@@@@@@@@@@@@@@,,,,,,,@@@''''@
                        // @@'@@@'''@@@@@@@@,,,,,,,,,,,,,,,,,,,,@@@@@@@'''@@@@'@
                        // @@'@,,@@@@@'''''''''@@@@@@@@@@@@@@''''''''@@@@@,,,@'@  original art by
                        // @@''@@@,,,,,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@,,,,,,@@@''@    coolcodecat.deviantart.com
                        // @@''@@'@@@@@@,,,,,,,,,,,,,,,,,,,,,,,,,,,@@@@@@''@@'@@  modified by r. kutina
                         // @@@,,@@@''''''@@@@@@@@@@@@@@@@@@@@@@@@'''''@@@@,,'@@  converted to ascii art with picascii.com
                         // @@ @@@,,,@@@@@@@@@''''''''''''''''@@@@@@@@,,,,@@@@@@
                          // @@'''@@@@,,,,,,,,,,@@@@@@@@@@@,,,,,,,,,,,@@@@''@@@
                            // @@@@''''@@@@@@@@@@@@@,,,,@@@@@@@@@@@@@''''@@@@
                               // @@@@@@@@@''''''''''''''''''''''@@@@@@@@

//
// Copyright (c) 2015 Richard Kutina, Spacecat Industries
//
// This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:
//
//	 1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.
//
//	 2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.
//
//	 3. This notice may not be removed or altered from any source distribution.
//

#define MAXMEM          0x8000		//32k of memory default
#define MAXBRP          16		//16 breakpoints is enough

#define ERR_OUTOFBOUNDS 2
#define ERR_DAFUQINSTR  3
#define ERR_OUTOFMEMORY 4
#define ERR_HALT        5
#define ERR_BREAKPOINT  6	        //that ain't really an error, but eh
#define ERR_STOPONINT   7               //that ain't really an error either

#define REG_JSP         8
#define REG_USP         9
#define REG_IA          10
#define REG_VER         11

#define COL_FRAME       1
#define COL_HIGHLIGHT   2
#define COL_MISC        3
#define COL_HELP        4
#define COL_WRITE       5
#define COL_READ        6
#define COL_ERROR       7
#define COL_INSTR       8 
#define COL_POINTER     9
#define COL_HALT        10
#define COL_BREAK       11
#define COL_INPUT       12
#define COL_ERRIN       13

#define PRINT_DECODED   1

void initcolors(){
	start_color();
	//        NAME           FOREGROUND    BACKGROUND
	init_pair(COL_FRAME,     COLOR_WHITE,  COLOR_CYAN);   //frame
	init_pair(COL_HIGHLIGHT, COLOR_BLACK,  COLOR_WHITE);  //regs etc
	init_pair(COL_MISC,      COLOR_WHITE,  COLOR_BLACK);  //other text
	init_pair(COL_HELP,      COLOR_YELLOW, COLOR_BLACK);  //help.
	init_pair(COL_WRITE,     COLOR_WHITE,  COLOR_RED);    //write
	init_pair(COL_READ,      COLOR_WHITE,  COLOR_GREEN);  //read
	init_pair(COL_ERROR,     COLOR_YELLOW, COLOR_RED);    //error
	init_pair(COL_INSTR,     COLOR_WHITE,  COLOR_YELLOW); //instrp
	init_pair(COL_POINTER,   COLOR_WHITE,  COLOR_BLUE);   //ptrs
	init_pair(COL_HALT,      COLOR_WHITE,  COLOR_GREEN);  //halted
	init_pair(COL_BREAK,     COLOR_WHITE, COLOR_MAGENTA); //breakpoint bar
	init_pair(COL_INPUT,     COLOR_BLUE,   COLOR_BLACK);  //input
	init_pair(COL_ERRIN,     COLOR_RED,    COLOR_BLACK);  //input error
}

void redraw();
void staticdraw();
void step();
void sig_handler();
void reset();
void cleanup();
void clearline();
void infomsg();
void errmsg();

int nextbp = 0;
int bp[MAXBRP] = {4};
int reg[16];
int carry, zero  = 0, inten = 0, intf  = 0;
int mrange = 0;
int usrint = 1;

long cyc   = 0;
int ic     = 0;
int previc = 0;

int rsrc1 = -1, rsrc2 = -1, rtgt = 1, op;

int mwadr  = -1, mradr  = -1;

int stopOnInt = 0;
int run = 0;

int instruction;

long adr, dat;

FILE * fp;

int mem[MAXMEM+1]={
	//FIBONACCI
	        //                      ; spacecat's fibonacci code in C pseudocode
	0x000C,	//zero	0               ; int prev = 0;
	0x040D,	//one	1	        ; int curr = 1;
	0x080C,	//zero	2               ; int tmp  = 0;
	0xEC20,	//ldll	3	32      ; int togo = 32;
	0x0800,	//move	2	0       ; do{
	        //                      ;     tmp  = prev;
	0x0080, //move	0	1       ;     prev = curr;
	0x0502,	//add	1	2       ;     curr += tmp;
	0xA410,	//push	1               ;     push(curr);
	0x0D8B,	//dec	3	3       ;     togo--;
	0x8500,	//siz                   ; 
	0x8004,	//jmp	4               ; }(while togo);                //I should've made this jump relative
	0xB000,	//halt                  ; //and we're done.
};

char inpbfr[80];

void inp_str(char * prestring){
	attron(COLOR_PAIR(COL_INPUT));
	timeout(-1); echo();
	move(23, 0); printw("%s ", prestring);
	getstr(inpbfr);
	timeout(0); noecho();
	move(23, 0); for(int i=0; i<80; i++) printw(" ");
}

long inp_hex(char * prestring){
int ok = 1;
        unsigned long res;
        inp_str(prestring);
        int len=0;
        for(int i=0; i<(sizeof(inpbfr)/sizeof(inpbfr[0])); i++){
                if(inpbfr[i] == 0){
                        len = i;
                        break;
                }
        }
        if(len > 8 || len == 0){
                ok = 0;
        }else{
	        for(int i=0; i<len; i++){
	                if(!((inpbfr[i] >= '0' && inpbfr[i] <= '9') || (inpbfr[i] >= 'a' && inpbfr[i] <= 'f') || (inpbfr[i] >= 'A' && inpbfr[i] <= 'F'))){
	                        ok = 0;
	                        break;
	                }
	        }
        }

        if(ok){
                res = strtoll(inpbfr, NULL, 16);    			       
        }else{
                res = -1;
        }
        return res;
}

int main(int argc, char *argv[]){
  int key = 0, lkey = -1;
  initscr();
  initcolors();
  cbreak();
  resize_term(24, 80);
  noecho();
  timeout(0);
  intrflush(stdscr, FALSE);
  keypad(stdscr, TRUE);
  
  reset();
  staticdraw();
  
  if(peri_check(MAXMEM)){
  	errmsg("Collision: Some peripherals are inside the memory range!"); 
  }
      
  while(key != 'q'){
  	lkey = key;
 	key = getch();
 	if(run == 1){
 		step();
 	}
 	
 	switch(key){
 		case 'a':	//mrange -= 8
 			mrange -= 8;
 			if(mrange < 0){
 				mrange = 0;
 			}
 			break;
 		case 'd':	//mrange += 8
 			mrange += 8;
 			if(mrange > (MAXMEM-0x20)){
 				mrange = MAXMEM-0x20;
 			}
 			break;
 		case 'A':	//mrange -= 32
 			mrange -= 32;
 			 if(mrange < 0){
 				mrange = 0;
 			}
 			break;
 		case 'D':	//mrange += 32
 			mrange += 32;
 			if(mrange > (MAXMEM-0x20)){
 				mrange = MAXMEM-0x20;
 			}
 			break;
 		case 's':	//step
 			if(run == 1 | run == 0){
				run = 0;
				step();
			}
 			break;
 		case 'S':       //step always
 			run = 0;
 			step();
 			break;
 		case 'o':       //save memory
 		        inp_str("Save MEM to:");
 		        if(inpbfr[0] != 0){
 		        	//TODO: fix endianess
 		                fp = fopen(inpbfr, "wb");
 		                fwrite(mem, sizeof(mem[0]), MAXMEM, fp);
 		                infomsg("Done.");
 		                fclose(fp);
 		        }
 		        break;
 		case 'l':       //load memory 
 		        inp_str("Read MEM from:");
 		        //TODO: fix endianess
 		        if( access( inpbfr, R_OK ) != -1 ){
         		        fp = fopen(inpbfr, "rb");
         		        fread(mem, sizeof(mem[0]), MAXMEM, fp);
         		        infomsg("Done.");
         		        fclose(fp);
 		        }else{
 		                attron(COLOR_PAIR(COL_ERRIN));
                                move(23, 0); printw("Can't find file %s", inpbfr);
 		        }
 		        break;
 		case 'O':
 		        inp_str("Save REG to:");
 		        fp = fopen(inpbfr, "wb");
 		        fwrite(reg, sizeof(mem[0]), 16, fp);
 		        infomsg("Done.");
 		        fclose(fp);
 		        break;
 		case 'L':
 		        inp_str("Read REG from:");
 		        if( access( inpbfr, R_OK ) != -1 ){
         		        fp = fopen(inpbfr, "rb");
         		        fread(reg, sizeof(mem[0]), 16, fp);
         		        infomsg("Done.");
         		        fclose(fp);
 		        }else{
 		                attron(COLOR_PAIR(COL_ERRIN));
                                move(23, 0); printw("Can't find file %s", inpbfr);
 		        }
 		        break;
 		case 'w':	//internal/user registers
 		        if(usrint){usrint = 0;}else{usrint=1;}
 			break;
 		case ' ':	//pause/run
 		case 'r':
 			if(run == 1){
 				run = 0;
 			}else{
 				if(run != 0){
 					reset();
 				}
 				run = 1;
 			}
 			break;
 		case 'i':	//interrupts
 			if(inten == 1){
 				intf = 1;
 			}
 			break;
 		case 'R':	//reset
 			reset();//intentionallymissingbreak
 		case 'u':
 			staticdraw();
 			break;
 		case 'I':
 			if(stopOnInt = 1){
 				stopOnInt = 0;
 			}else{
 				stopOnInt = 1;
 			}
 			break;
 		case 'b':
         		attron(COLOR_PAIR(COL_INPUT));
 		        if(nextbp < MAXBRP){
 		                long b;
 		                if((b = inp_hex("New BP:")) != -1){
 		                        bp[nextbp] = b;
 		                        nextbp++;
 		                }else{
 		                        attron(COLOR_PAIR(COL_ERRIN));
                                        infomsg("Try again.");
 		                }
 			}else{
 			        attron(COLOR_PAIR(COL_ERRIN));
                                infomsg("Out of breakpoints! D:");
 			}
 			break;
 		case 'B':
 			nextbp=0;
 			break;
 	        case 'm':       //edit memory
 	                attron(COLOR_PAIR(COL_INPUT));
 		        if((adr = inp_hex("MEM Address:")) != -1){
 		                if(adr >= MAXMEM || adr < 0){	//TODO: add possibility for devices to be outside of memory range
 		                        attron(COLOR_PAIR(COL_ERRIN));
                                        infomsg("Out of memory range!");
 		                        break;
 		                }
 		                if((dat = inp_hex("Data:")) != -1){
 		                        mem[adr] = dat;
 		                        infomsg("Done.");
 		                }else{
 		                        attron(COLOR_PAIR(COL_ERRIN));
                                        infomsg("Try again.");
 		                }
	                }else{
	                        attron(COLOR_PAIR(COL_ERRIN));
                                infomsg("Try again.");
	                }
 			break;
 		case 'M':       //edit registers
 	                attron(COLOR_PAIR(COL_INPUT));
 		        if((adr = inp_hex("REG Address:")) != -1){
 		                if(adr >= 16 || adr < 0){
 		                        attron(COLOR_PAIR(COL_ERRIN));
                                        infomsg("Out of reg range!");
 		                        break;
 		                }
 		                if((dat = inp_hex("Data:")) != -1){
 		                        reg[adr] = dat;
 		                        infomsg("Done.");
 		                }else{
 		                        attron(COLOR_PAIR(COL_ERRIN));
                                        infomsg("Try again.");
 		                }
	                }else{
	                        attron(COLOR_PAIR(COL_ERRIN));
                                infomsg("Try again.");
	                }
 			break;
 		default:
 			break;
 	}
  	redraw();
  }
  cleanup();
  return 0;
}

void step(){
        reg[8+3] = 0;
	previc = ic;
	
	if(intf){
		inten = 0;
		ic = reg[REG_IA];
		if(stopOnInt){
			run = ERR_STOPONINT;
		}
	}
	
	if(ic>=MAXMEM){
 		run = ERR_OUTOFBOUNDS;
 	}else{
 		for(int i=0; i<nextbp; i++){
 			if(ic == bp[i]){
 				run = ERR_BREAKPOINT;
 			}
 		}
 		
		cyc++;
		instruction = mem[ic] & 0xFFFF;
		rsrc1 = -1;
		rsrc2 = -1;
		rtgt  = -1;
		mradr = -1;
		mwadr = -1;
		//III- ---- ---- ---- = >> 13
		char in = 7&(instruction >> 13);
		switch(in){
			case 0:         //ALU instructions	//000- ---- ---- ---- = >> 0
				rtgt  = 7&(instruction >> 10);	//---I II-- ---- ---- = >> 10
				rsrc1 = rtgt;
				rsrc2 = 7&(instruction >> 7);	//---- --II I--- ---- = >> 7
				op    = 127 & instruction;	//---- ---- -III IIII = >> 0
				carry = 0;
				int prev = reg[rtgt];
				switch(op){
					case  0: reg[rtgt] =  reg[rsrc2]; break;	//move
					case  1: reg[rtgt] = ~reg[rsrc2]; break;	//not
					case  2: reg[rtgt] += reg[rsrc2];		//add
						 if(reg[rtgt] < prev){
						 	carry = 1;
						 }
						 break;
					case  3: reg[rtgt] -= reg[rsrc2];		//sub
					 	 if(reg[rtgt] > prev){
						 	carry = 1;
						 }
						 break;
					case  4: reg[rtgt] &= reg[rsrc2]; break;	//and
					case  5: reg[rtgt] = ~(reg[rsrc1] & reg[rsrc2]); break;	//nand
					case  6: reg[rtgt] |= reg[rsrc2]; break;	//or
					case  7: reg[rtgt] = ~(reg[rsrc1] | reg[rsrc2]); break;	//nor
					case  8: reg[rtgt] ^= reg[rsrc2]; break;	//xor (do not confuse with haxor)
					case  9: reg[rtgt] = ~(reg[rsrc1] ^ reg[rsrc2]); break;	//xnor
					case 10: reg[rtgt]++;    			//inc
						 if(reg[rtgt] < prev){
						 	carry = 1;
						 }
						 break;
					case 11: reg[rtgt]--; break;			//dec
						if(reg[rtgt] > prev){
						 	carry = 1;
						 }
						 break;
					case 12: reg[rtgt] = 0; break;			//zero
					case 13: reg[rtgt] = 1; break;			//one
					case 14: reg[rtgt] =-1; carry=1; break;		//monec
					case 15: reg[rtgt] =-1; break;			//mone
					default: run = ERR_DAFUQINSTR; break;		//it's broken
				}
				zero = (reg[rtgt] == 0);
				ic++;
				break;
			case 1:         //BSU instructions	//001- ---- ---- ---- = >> 0
				rtgt  = 7&(instruction >> 10);	//---I II-- ---- ---- = >> 10
				rsrc1 = 7&(instruction >> 7);	//---- --II I--- ---- = >> 7
				op    = 7&(instruction >> 4);	//---- ---- -III ---- = >> 4
				int c = instruction&4;		//---- ---- ---- IIII = >> 0
				if(op&4){
					rsrc2 = c&7;
					c = rsrc2;
					op = op&3;
				}
				switch(op){
					case 0:	reg[rtgt] = reg[rsrc1] << (c+1);	//SHL - shift left
					case 1: reg[rtgt] = reg[rsrc1] >> (c+1);	//SHL - shift right
					case 2: reg[rtgt] = (reg[rsrc1] << (c+1)) | (reg[rsrc1] >> (c+31));	//ROL - roll left
					case 3: reg[rtgt] = (reg[rsrc1] << (c+1)) | (reg[rsrc1] >> (c+31));	//ROR - roll right
					default: run = ERR_DAFUQINSTR; break;
				}
				ic++;
				break;
			case 2: case 3: //direct jumps	//01X- ---- ---- ---- = >> 0
				ic = ((ic & 0xC000) | (instruction & 0x3FFF));	//--II IIII IIII IIII = >> 0
				break;
			case 4:         //conditional jumps, calls, rets...
			case 5:
							//100- ---- ---- ---- = >> 0
				switch(63&(instruction >> 8)){  //--II IIII ---- ---- = >> 10
					case 0b000000:	//jump
					case 0b000001:
					case 0b000010:
					case 0b000011:
						ic = ((ic & 0xFC00) | (instruction & 0x03FF));	//---- --II IIII IIII = >> 0
						break;
					case 0b000100:	//SIC
						if(carry){ ic+=2; }else{ ic++; }
						break;
					case 0b000101:	//SIZ
						if(zero){ ic+=2; }else{ ic++; }
						break;
					case 0b000110:	//SIH
					case 0b000111:	//SIL
						rsrc1 = (instruction&7);			//---- ---- ---- -III = >> 0
						int n = 31&(instruction>>3);			//---- ---- IIII I--- = >> 3
						if((reg[rsrc1]>>n)&1 == (instruction>>8)&1){	//---- ---I ---- ---- = >> 8
							ic++;	
						}else{
							ic+=1;
						}
						break;
					case 0b001000: case 0b001001: case 0b001010: case 0b001011: case 0b001100:
					case 0b001101: case 0b001110: case 0b001111: //RJMP
						ic = (ic&0xF800) + (instruction&0x07FF) - 0x03ff;
						break;
					case 0b010110: case 0b010111:	//RETIE
						inten = 1;
        				case 0b010100: case 0b010101:	//RET
						reg[REG_JSP] = reg[REG_JSP]-1;
						ic = mem[reg[REG_JSP]];
						break;
					case 0b011000: case 0b011001: case 0b011010: case 0b011011: //CALL
						mem[reg[REG_JSP]]=ic;
						ic = (ic&0xFC00) + (instruction&0x03FF);
						reg[REG_JSP] = reg[REG_JSP]+1;
						break;
					case 0b011100: case 0b011101: case 0b011110: case 0b011111: //RCALL
						mem[reg[REG_JSP]]=ic;
						ic = (ic&0xFC00) + (instruction&0x03FF) - 0x01ff;
						reg[REG_JSP] = reg[REG_JSP]+1;
						break;
					case 0b100000: case 0b100001: case 0b100010: case 0b100011: //OUT
						rsrc1 = (instruction>>4)&7;
						rsrc2 = instruction&7;
						mwadr = reg[rsrc2];
						if(mwadr >= MAXMEM){
							peri_upd(mwadr, reg[rsrc1], PERI_WRITE);
						}else{
							mem[mwadr] = reg[rsrc1];
						}
						ic++;
						break;
					case 0b100100: case 0b100101: case 0b100110: case 0b100111: //PUSH
						rsrc1 = (instruction>>4)&7;
						mwadr = reg[REG_USP];
						mem[mwadr] = reg[rsrc1];
						if(mwadr >= MAXMEM){
							peri_upd(mwadr, reg[rsrc1], PERI_WRITE);
						}else{
							mem[mwadr] = reg[rsrc1];
						}
						reg[REG_USP] = reg[REG_USP]+1;
						ic++;
						break;
 					case 0b101000: case 0b101001: case 0b101010: case 0b101011: //IN
						rtgt = (instruction>>4)&7;
						rsrc2 = instruction&7;
						mradr = reg[rsrc2];
						if(mradr >= MAXMEM){
							reg[rtgt] = peri_upd(mradr, reg[rsrc1], PERI_READ);
						}else{
							reg[rtgt] = mem[mradr];
						}
						ic++;
						break;
					case 0b101100: case 0b101101: case 0b101110: case 0b101111: //POP
						reg[REG_USP] = reg[REG_USP]-1;
						rtgt = (instruction>>4)&7;
						mradr = reg[REG_USP];
						if(mradr >= MAXMEM){
							reg[rtgt] = peri_upd(mradr, reg[rsrc1], PERI_READ);
						}else{
							reg[rtgt] = mem[mradr];
						}
						ic++;
						break;
					case 0b110000:	//HALT
						run = ERR_HALT;
						ic++;
						break;
					case 0b111000:	//INTE
						inten = 1;
						ic++;
						break;
					case 0b111001:	//INTD
						inten = 0;
						ic++;
						break;
					case 0b111010:	//RDINT
						rtgt = (instruction>>4)&7;
						rsrc2 = 8|(instruction&7);
						reg[rtgt] = reg[rsrc2];
						ic++;
						break;
					case 0b111011:	//WRINT
						rsrc1 = (instruction>>4)&7;
						rtgt = 8|(instruction&7);
						reg[rtgt] = reg[rsrc1];
						ic++;
						break;
					case 0b111100:	//JMPR
						rsrc1 = instruction&7;
						ic = reg[rsrc1];
						break;
					case 0b111101:	//CALLR
						rsrc1 = instruction&7;
						mem[reg[REG_JSP]]=ic;
						ic = reg[rsrc1];
						reg[REG_JSP] = reg[REG_JSP]+1;
						break;
					default: run = ERR_DAFUQINSTR; break;
				}
				break;
			case 6:         //config	//110- ---- ---- ---- = >> 0
			case 7:         //reg. load	//111- ---- ---- ---- = >> 0
				//---I II-- ---- ---- = >> 10
				if(in == 6){
					rsrc1 = 8|(7&(instruction >> 10));
				}else{
					rsrc1 = 7&(instruction >> 10);
				}
				rtgt  = rsrc1;
				rsrc2 = -1;
				//---- ---- ---- ---- = >> 8
				switch(3&(instruction >> 8)){
					case 0: reg[rsrc1] = ((reg[rsrc1] & 0xFFFFFF00) | (instruction & 0x00FF)); break;	//LL
					case 1: reg[rsrc1] = ((reg[rsrc1] & 0xFFFF00FF) | ((instruction & 0x00FF)<<8));	break;	//LH
					case 2: reg[rsrc1] = ((reg[rsrc1] & 0xFF00FFFF) | ((instruction & 0x00FF)<<16)); break;	//HL
					case 3: reg[rsrc1] = ((reg[rsrc1] & 0x00FFFFFF) | ((instruction & 0x00FF)<<24)); break;	//HH
					default: run = ERR_DAFUQINSTR; break;
				}
				ic++;
				break;
			default:
				run = ERR_DAFUQINSTR;
				break;
		}
		
		peri_upd(0,0,PERI_UPDATE);
	}	
}

void reset(){
  cyc   =  0;
  ic    =  0;
  previc=  0;
  
  rsrc1 = -1;
  rsrc2 = -1;
  rtgt  = -1;

  mradr = -1;
  mwadr = -1;

  run   =  0;

  carry =  0;
  zero  =  0;
  inten =  0;
  intf  =  0;
  
  for(int i=0; i<16; i++) reg[i] = 0;
  
  reg[REG_USP] = 0x100;
}

void staticdraw(){

  attron(COLOR_PAIR(COL_FRAME));
  clearline(0);
  move(0, 0); 	printw(" < Spacecat #30 Emulator >");
  move(0, 64);	printw(" ( v20150418 )");
                           
  clearline(8);
  move(8, 1); 	printw("Memory");
  move(8, 57);	printw("Avail.: %08x (%uK)", MAXMEM, MAXMEM/1024);

  attron(COLOR_PAIR(COL_HIGHLIGHT));
  move(4, 56);	printw("JSP:");
  move(5, 56);	printw("    ");
  move(6, 56);	printw("USP:");
  move(7, 56);	printw("    ");
  
  attron(COLOR_PAIR(COL_MISC));
  clearline(18);
  clearline(19);
  clearline(20);
  clearline(21);
  
  attron(COLOR_PAIR(COL_HELP));
  move(18, 0); printw(" [a/d] mem range +/- 8  [s] step       [r,space] run/stop   [w] usr/int r.");
  move(19, 0); printw(" [A/D] mem range +/-32  [i] interrupt  [I] stop on int      [b] set break.");
  move(20, 0); printw(" [M] modify register    [R] reset      [m] modify memory    [B] clr break.");
  move(21, 0); printw(" [o] save memory        [l] load mem.  [O] save regs        [L] load regs");
  
  move(0,0);
}

void redraw(){
    
    
  attron(COLOR_PAIR(COL_FRAME));
    
  move(8, 7);
  printw("(%08x - %08x):", mrange, mrange+0x1f, MAXMEM, MAXMEM/1024);

  clearline(3);
  move(3, 1); 
  if(usrint){
        printw("Registers (USR):");
  }else{
        printw("Registers (INT):");
  }

  switch(run){
          case 0: case 1:      attron(COLOR_PAIR(COL_FRAME));   break;
          case ERR_HALT:       attron(COLOR_PAIR(COL_POINTER)); break;
          case ERR_BREAKPOINT: attron(COLOR_PAIR(COL_BREAK));   break;
          default:             attron(COLOR_PAIR(COL_ERROR));   break;
  }
    
  clearline(17);

  move(17, 1);
  switch(run){
  	case 1:               printw("Running...");                                break;
  	case 0:               printw("Stopped.");                                  break;
	case ERR_OUTOFBOUNDS: printw("Stopped! Error: out of bounds!");            break;
	case ERR_DAFUQINSTR:  printw("Stopped! Error: unrecognized instruction!"); break;
	case ERR_OUTOFMEMORY: printw("Stopped! Error: out of memory!");            break;
	case ERR_HALT:        printw("Halted.");                                   break;
	case ERR_BREAKPOINT:  printw("Break");                                     break;
	default:              printw("For some reason it stopped. D:");            break;
  }
  
  attron(COLOR_PAIR(COL_HIGHLIGHT));
  for(int i=0; i<8; i+=2){
  	if(i == rsrc1 || i == rsrc2 || i == rtgt){
  		move(4+i/2, 0);
  		if(i == rtgt){
  			if(rsrc1 == rtgt | rsrc2 == rtgt){
  				attron(COLOR_PAIR(COL_READ));
  			}else{
  				attron(COLOR_PAIR(COL_WRITE));
  			}
  			printw("WR%x:", i);
  		}else if(i == rsrc2){
  			attron(COLOR_PAIR(COL_READ));
  			printw("BR%x:", i);
  		}else{
  			attron(COLOR_PAIR(COL_READ));
  			if(rsrc1 != rsrc2){
  				printw("AR%x:", i);
  			}else{
	  			printw("XR%x:", i);
  			}
  		}
  		attron(COLOR_PAIR(COL_HIGHLIGHT));
  	}else{
	  	attron(COLOR_PAIR(COL_HIGHLIGHT));
  	  	move(4+i/2, 0);	printw("-R%x:", i);
  	}
  	
  	if(i+1 == rsrc1 || i+1 == rsrc2 || i+1 == rtgt){
  	  	move(4+i/2, 28);
  	  	//sct_setfgc(7);
  		if(i+1 == rtgt){
  			if(rsrc1 == rtgt | rsrc2 == rtgt){
  				attron(COLOR_PAIR(COL_READ));
  			}else{
  				attron(COLOR_PAIR(COL_WRITE));
  			}
  			printw("WR%x:", i+1);
  		}else if(i+1 == rsrc2){
  			attron(COLOR_PAIR(COL_READ));
  			printw("BR%x:", i+1);
  		}else{
			attron(COLOR_PAIR(COL_READ));
  			if(rsrc1 != rsrc2){
  				printw("AR%x:", i+1);
  			}else{
	  			printw("XR%x:", i+1);
  			}
  		}
		attron(COLOR_PAIR(COL_HIGHLIGHT));  		
  	}else{
	  	attron(COLOR_PAIR(COL_HIGHLIGHT));
  		move(4+i/2, 28); printw("-R%x:", i+1);
  	}
  }
  
  for(int i=0; i<8; i++){
	for(int j = 0; j<4; j++){
  		move(9+i, 20*j);
  		printw(" %08x", mrange+(8*j+i));
  	}
  }

  attron(COLOR_PAIR(COL_MISC));
  
  for(int i=0; i<8; i++){
	for(int j = 0; j<4; j++){
  		move(9+i, 10+20*j);
  		printw("%08x", mem[mrange+(8*j+i)]);
  	}
  }
  
  move(1, 9); 	printw("%lu\n", cyc);	//cycle
  
  move(2, 9); 	printw("%04x", instruction);

  move(1, 51);	printw("%u (%08x)\n", previc, previc);	//address
  
  if(usrint){
          for(int i=0; i<8; i+=2){
            move(4+i/2, 5);	printw("           (        )");
            move(4+i/2, 5);	printw("%d", reg[i]);
            move(4+i/2, 16);	printw("(%08x)", reg[i]);
            move(4+i/2, 33);	printw("           (        )");
            move(4+i/2, 33);	printw("%d", reg[i+1]);
            move(4+i/2, 44);	printw("(%08x)", reg[i+1]);
          }
  }else{
         for(int i=0; i<8; i+=2){
            move(4+i/2, 5);	printw("           (        )");
            move(4+i/2, 5);	printw("%d", reg[8+i]);
            move(4+i/2, 16);	printw("(%08x)", reg[i]);
            move(4+i/2, 33);	printw("           (        )");
            move(4+i/2, 33);	printw("%d", reg[9+i]);
            move(4+i/2, 44);	printw("(%08x)", reg[i+1]);
          }
  }
  
  move(2, 51); printw("C:%d Z:%d IE:%d IF:%d", carry, zero, inten, intf);
  
  move(4, 61);	printw("%u",   reg[REG_JSP]);
  move(5, 61);	printw("(%x)", reg[REG_JSP]);
  move(6, 61);	printw("%u",   reg[REG_USP]);
  move(7, 61);	printw("(%x)", reg[REG_USP]);
  
  if(mwadr != mradr){
	  if(mradr >= mrange && mradr < mrange + 0x1f){
	  	move(9+(mradr-mrange)%8, 20*(((mradr-mrange)/8)%4));
	  	  attron(COLOR_PAIR(COL_READ));	//read
	  	  printw("R");
	  }
	  
	  if(mwadr >= mrange && mwadr < mrange + 0x1f){
	  	move(9+(mwadr-mrange)%8, 20*(((mwadr-mrange)/8)%4));
	  	  attron(COLOR_PAIR(COL_WRITE));	//read
	  	  printw("W");
	  }
  }else{
  	  if(mradr >= mrange && mradr < mrange + 0x1f){
	  	move(9+(mradr-mrange)%8, 20*(((mradr-mrange)/8)%4));
	  	  attron(COLOR_PAIR(COL_READ));	//read
	  	  printw("X");
	  }
  }

  
  if(reg[REG_JSP] >= mrange && reg[REG_JSP] < mrange + 0x1f){
  	move(9+(reg[REG_JSP]-mrange)%8, 20*(((reg[REG_JSP]-mrange)/8)%4));
	attron(COLOR_PAIR(COL_POINTER));	//>
	printw("J");
  }
  
  if(reg[REG_USP] >= mrange && reg[REG_USP] < mrange + 0x1f){
  	move(9+(reg[REG_USP]-mrange)%8, 20*(((reg[REG_USP]-mrange)/8)%4));
	attron(COLOR_PAIR(COL_POINTER));	//>
	printw("U");
  }
  
  if(reg[REG_IA] >= mrange && reg[REG_IA] < mrange + 0x1f){
  	move(9+(reg[REG_IA]-mrange)%8, 20*(((reg[REG_IA]-mrange)/8)%4));
	attron(COLOR_PAIR(COL_POINTER));	//>
	printw("I");
  }
  
  if(previc >= mrange && previc < mrange + 0x1f){
  	move(9+(previc-mrange)%8, 20*(((previc-mrange)/8)%4));
	attron(COLOR_PAIR(COL_INSTR));	//>
	printw(">");
  }
  
  for(int i=0; i<nextbp; i++){
    	if(bp[i] >= mrange && bp[i] < mrange + 0x1f){
  		move(9+(bp[i]-mrange)%8, 20*(((bp[i]-mrange)/8)%4));
		attron(COLOR_PAIR(COL_BREAK));	//>
		if(bp[i] == ic){
			printw("!");
		}else{
			printw("B");
		}
  	}
  }

  attron(COLOR_PAIR(COL_HIGHLIGHT));
  move(1, 41);	printw(" Address:");
  move(2, 41);	printw(" Flags  :");
  move(1, 0); 	printw(" Cycle#:");
  move(2, 0); 	printw(" Instr.:");  
  
  move(0, 0);
}

void cleanup(){
        endwin();
        exit(0);
}

void clearline(int y){
        move(y, 0);
        for(int i = 0; i<80; i++){
                printw(" ");    
        }
}

void infomsg(char * str){
        clearline(23);
        move(23, 0); printw(str);
}

void errmsg(char * str){
	attron(COLOR_PAIR(COL_ERROR));
	infomsg(str);
}
