#ifndef PERIINT
#define PERIINT

#define PERI_READ      0	//for actual device operation
#define PERI_WRITE     1	//for actual device operation
#define PERI_UPDATE   16	//just update the thing, some devices might need this (timers, serials...)
#define PERI_DEADREAD 24	//just read the output of it, but don't do anything else
                        	//why the f is it there? some devices might do something like pop their buffer or so when read
                        	// this one shouldn't cause that, because I want to see the devices in the
                        	// memory view anyway

unsigned int peri_upd(unsigned int adr, unsigned int dat, char mode);
int peri_check(int);

#endif
