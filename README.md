# sc30emu
Spacecat SC30 processor emulator

sc30emu is a very simple emulator of the silly SC30 processor architecture I've made.
some more info is at http://spacecatindustries.org/?p=sc30

## Features
* Basic ~SC30.0 emulation
* Memory view
* Register view
* Interrupts

## Todo
* Add some peripherals, hopefully configurable
* Add interactive peripherals, such as a terminal
* Add more run modes
* Batch run (nogui) mode
* Make it _REALLY_ comply with SC30.0 spec
* Write an usage tutorial
