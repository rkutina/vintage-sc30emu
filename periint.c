//Peripheral Interface
#include "periint.h"

unsigned int dummy(
	unsigned int badr,	//device base address
	unsigned int adr,	//device local address
	unsigned int dat,	//data in
	char mode){		//mode, 0 = read, 1 = write, 16 = update only
	return 1;
}

//used for peripheral callbacks:
typedef unsigned int(*p_cb)(
	unsigned int,	//device base (offset) address
	unsigned int,	//device local address
	unsigned int,	//data in
	char);		//mode; 0 = read, 1 = write, 16 = update, 24 = read(nondestructive)

typedef struct {
	p_cb          func;	//interface function for a peripheral
	unsigned int  badr;	//base address
}s_peripheral;

s_peripheral peripherals[] = {
	{
		.func = &dummy,
		.badr = 0x8000,
	}
};

//update peripherals, read/write
unsigned int peri_upd(unsigned int adr, unsigned int dat, char mode){
	unsigned int ret=0;
	s_peripheral p;
	for(int i = 0; i < (sizeof(peripherals)/sizeof(s_peripheral)); i++){
		p = peripherals[i];
		int temp_ret = p.func(p.badr, adr, dat, mode);
		if(mode == PERI_READ && temp_ret){
			ret = temp_ret;
		}
	}
	return ret;
}

int peri_check(int maxmem){
	for(int i = 0; i < (sizeof(peripherals)/sizeof(s_peripheral)); i++){
		if(peripherals[i].badr < maxmem) return 1;
	}
	return 0;
}
